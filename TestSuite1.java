import static org.junit.Assert.*;
import org.junit.Test;

public class TestSuite1 {

    @Test
    public void testValidSSN() {
        assertTrue(CheckSSN.isValidSSN("111-22-3333"));
    }

    @Test
    public void testInvalidSSN1() {
        assertFalse(CheckSSN.isValidSSN("11122-3333"));
    }
    @Test
    public void testInvalidSSN2() {
        assertFalse(CheckSSN.isValidSSN("111-223333"));
    }
    @Test
    public void testInvalidSSN3() {
        assertFalse(CheckSSN.isValidSSN("111223333"));
    }
    @Test
    public void testInvalidSSN4() {
        assertFalse(CheckSSN.isValidSSN("1111-22-3333"));
    }


}
